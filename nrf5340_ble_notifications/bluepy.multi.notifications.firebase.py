from bluepy import btle
import struct
import threading

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

from bluepy.btle import BTLEDisconnectError

# warning! this code has two inifinte while loops! ;)

VV_FIREBASE_NRF5340_DB_URL = 'https://vv-firebase-nrf5340-default-rtdb.firebaseio.com/'
GOOGLE_SERVICE_ACCOUNT_CERT_PATH = "vv-firebase-nrf5340-firebase-adminsdk-lepad-f0eadf5f5b.json"
ENABLE_DATA_UPLOAD = False


# init db client to push new data
cred = credentials.Certificate( GOOGLE_SERVICE_ACCOUNT_CERT_PATH )
firebase_admin.initialize_app(cred,  {'databaseURL': VV_FIREBASE_NRF5340_DB_URL})
ref = db.reference('nrf5340_ble_endpoint')


class MyDelegate(btle.DefaultDelegate):
    def __init__(self, params):
        btle.DefaultDelegate.__init__(self)
        self._meta = "vv.meta.0"
        self.ble_hw_uuid = params["ble_hw_uuid"]
	# ... initialise here

    def handleNotification(self, cHandle, data):
        print(self.ble_hw_uuid + " data=%s\n", str(data))
        if ENABLE_DATA_UPLOAD: 
            ref.child(self.ble_hw_uuid).set({ self.ble_hw_uuid : str(data) })

# Initialisation  -------

def init_and_get_notification( ble_hw_uuid ):
    p = btle.Peripheral( ble_hw_uuid, "random" )
    print("peripheral init success!\n");
    p.setMTU(1024)
    p.setDelegate( MyDelegate({"ble_hw_uuid" : ble_hw_uuid}) )

    # Setup to turn notifications on, e.g.
    svc = p.getServiceByUUID( "12345678-1234-5678-1234-5678deadbeef" )
    print(str(svc.getCharacteristics()))
    ch_notify = svc.getCharacteristics()[0]
    p.writeCharacteristic(ch_notify.valHandle+1, b'\x01\x00', withResponse=True)

    config_gatt_char = svc.getCharacteristics()[1] 
    send_msg_bytes =bytes("Hello world\n", "utf-8")
    config_gatt_char.write(send_msg_bytes)

    while True:
        if p.waitForNotifications(1.0):
            # handleNotification() was called
            continue

        print("Waiting...")
    # Perhaps do something else here

def thread_function_capsule( ble_peripheral_uuid ):
    while(True):
        try:
            init_and_get_notification( ble_peripheral_uuid )
        except BTLEDisconnectError as ble_disconnect_error:
            print("ble disconnect error!")


threads = []
ble_peripheral_uuid_list = ["E3:3E:62:FB:A5:92", "D2:0E:8F:87:98:62"]
for ble_peripheral_uuid in ble_peripheral_uuid_list:
    print(ble_peripheral_uuid)
    t = threading.Thread(target=thread_function_capsule, args=(ble_peripheral_uuid,))
    threads.append(t)
    t.start()


#init_and_get_notification("E3:3E:62:FB:A5:92")
