

import sys
from bluepy.btle import UUID, Peripheral
import time
if len(sys.argv) != 2:
  print("Fatal, must pass device address:", sys.argv[0], "<device address="">")
  quit()

start = time.time()

p = Peripheral(sys.argv[1],"random")
while(1):
  chList = p.getCharacteristics()
  print("Handle   UUID                                Properties")
  print("-------------------------------------------------------")                      
  for ch in chList:
    print(("  0x"+ format(ch.getHandle(),'02X')  +"   "+str(ch.uuid) +" " + ch.propertiesToString()))
  time.sleep(1)

  end = time.time()
  print("connected for time= %sseconds", str((end - start)))
