from bluepy import btle
import struct
class MyDelegate(btle.DefaultDelegate):
    def __init__(self, params):
        btle.DefaultDelegate.__init__(self)
        self._meta = "vv.meta.0"
	# ... initialise here

    def handleNotification(self, cHandle, data):
        print("data=%s\n", str(data))
	# ... perhaps check cHandle
        # ... process 'data'


# Initialisation  -------

p = btle.Peripheral( "EA:45:D2:25:C0:5A", "random" )
print("peripheral init success!\n");
p.setMTU(1024)
p.setDelegate( MyDelegate({}) )

# Setup to turn notifications on, e.g.
svc = p.getServiceByUUID( "12345678-1234-5678-1234-5678deadbeef" )
print(str(svc.getCharacteristics()))
ch_notify = svc.getCharacteristics()[0]
p.writeCharacteristic(ch_notify.valHandle+1, b'\x01\x00', withResponse=True)

config_gatt_char = svc.getCharacteristics()[1] 
send_msg_bytes =bytes("Hello world\n", "utf-8")
config_gatt_char.write(send_msg_bytes)


print("wrote the characteristic!")


#ch = svc.getCharacteristics( "12345678-1234-5678-1234-56789abcdef0" )[0]
#ch.write(struct.pack('<bb', 0x01, 0x00))

# Main loop --------

while True:
    if p.waitForNotifications(1.0):
        # handleNotification() was called
        continue

    print("Waiting...")
    # Perhaps do something else here
