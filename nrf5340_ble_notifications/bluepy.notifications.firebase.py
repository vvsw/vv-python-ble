from bluepy import btle
import struct
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


ble_peripheral_uuid = "D2:0E:8F:87:98:62"

VV_FIREBASE_NRF5340_DB_URL = 'https://vv-firebase-nrf5340-default-rtdb.firebaseio.com/'
GOOGLE_SERVICE_ACCOUNT_CERT_PATH = "vv-firebase-nrf5340-firebase-adminsdk-lepad-f0eadf5f5b.json"

# init db client to push new data
cred = credentials.Certificate( GOOGLE_SERVICE_ACCOUNT_CERT_PATH )
firebase_admin.initialize_app(cred,  {'databaseURL': VV_FIREBASE_NRF5340_DB_URL})
ref = db.reference('ble+D2:0E:8F:87:98:62')


class MyDelegate(btle.DefaultDelegate):
    def __init__(self, params):
        btle.DefaultDelegate.__init__(self)
        self._meta = "vv.meta.0"
	# ... initialise heres

    def handleNotification(self, cHandle, data):
        print("data=%s\n", str(data))
        ref.set({
            ble_peripheral_uuid: str(data)
        })



	# ... perhaps check cHandle
        # ... process 'data'


# Initialisation  -------

p = btle.Peripheral( ble_peripheral_uuid , "random" )
print("peripheral init success!\n");
p.setMTU(1024)
p.setDelegate( MyDelegate({}) )

# Setup to turn notifications on, e.g.
svc = p.getServiceByUUID( "12345678-1234-5678-1234-5678deadbeef" )
print(str(svc.getCharacteristics()))
ch_notify = svc.getCharacteristics()[0]
p.writeCharacteristic(ch_notify.valHandle+1, b'\x01\x00', withResponse=True)

config_gatt_char = svc.getCharacteristics()[1] 
send_msg_bytes =bytes("Hello world\n", "utf-8")
config_gatt_char.write(send_msg_bytes)


print("wrote the characteristic!")


#ch = svc.getCharacteristics( "12345678-1234-5678-1234-56789abcdef0" )[0]
#ch.write(struct.pack('<bb', 0x01, 0x00))

# Main loop --------

while True:
    if p.waitForNotifications(1.0):
        # handleNotification() was called
        continue

    print("Waiting...")
    # Perhaps do something else here
