# -*- coding: utf-8 -*-
"""
Notifications example for NSP32 XL500 BLE, Auto-Exposure On

Requirements:
- bleak python library ( $ pip install bleak )


Notes:
- Throws warning about asyncio 'Future <Future pending> attached to a different loop'


April 26, 2022 - info@nanolambda.net
"""

import sys
import asyncio
import platform
import array
import matplotlib.pyplot as plt
from bleak import BleakClient,BleakScanner

TARGET_DEVICE_NAME = "NSP32_SPECTRUM"
# you can change these to match your device or override them from the command line
SERVICE_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca3e"#.upper()
RX_CHARACTERISTIC_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca3e"
NOTIFY_CHARACTERISTIC_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca3e".upper()
AUTOEXPOSURE_ENABLED = True

lock = asyncio.Lock()
client = None
ble_rx_characteristic = None

def get_checksum_value(buf):
    sum = 0
    for i in range(0, len(buf) - 1):
        sum = sum + int.from_bytes(buf[i], "big")
    checksum = (((~sum) + 1) & 0xFF);
    return checksum
    

spectrum_bytes_data = []
acquire_spectrum_data_chunk_number = 0

async def notification_handler(sender, data):
    global acquire_spectrum_data_chunk_number
    global spectrum_bytes_data
    global client
    global ble_rx_characteristic
    acquire_spectrum_data_chunk_number = acquire_spectrum_data_chunk_number + 1
    if(acquire_spectrum_data_chunk_number == 3):

        ar = array.array('f', data[:-1])
        a = ar.tolist()
        for i in range(len(a)):
            if a[i] < 0:
                a[i] = 0
        spectrum_bytes_data = spectrum_bytes_data + a

        spectrum_bytes_data = spectrum_bytes_data[3:-3]
        assert(len(spectrum_bytes_data) == 135)
        plt.plot(range(len(spectrum_bytes_data)),spectrum_bytes_data)
        plt.show()
        spectrum_bytes_data = []
        acquire_spectrum_data_chunk_number = 0
        await send_nsp32_ble_command(client, ble_rx_characteristic)
    else:
        ar = array.array('f', data)
        a = ar.tolist()
        for i in range(len(a)):
            if a[i] < 0:
                a[i] = 0
        spectrum_bytes_data = spectrum_bytes_data + a

        



async def send_nsp32_ble_command(client, ble_rx_characteristic):
    async with lock:
        # hello_cmd_hex = 0x01
        # standby_cmd_hex = 0x04
        # get_sensor_id_cmd_hex = 0x06 
        acquire_spectrum_cmd_hex = 0x26
        acquire_spectrum_cmd_length = 10
        acquire_spectrum_user_code_hex = 0x00

        acquire_spectrum_integration_time = 100
        acquire_spectrum_frame_average_number = 15
        acquire_spectrum_enable_auto_exposure = AUTOEXPOSURE_ENABLED

        command_send_buffer = [0] * acquire_spectrum_cmd_length
        command_send_buffer[0] = 0x03.to_bytes(1, byteorder='big')
        command_send_buffer[1] = 0xBB.to_bytes(1, byteorder='big')
        command_send_buffer[2] = acquire_spectrum_cmd_hex.to_bytes(1, byteorder='big')
        command_send_buffer[3] = acquire_spectrum_user_code_hex.to_bytes(1, byteorder='big')
        command_send_buffer[4] = (acquire_spectrum_integration_time & 0xFF).to_bytes(1, byteorder='big')
        command_send_buffer[5] = ((acquire_spectrum_integration_time >> 8) & 0xFF).to_bytes(1, byteorder='big')
        command_send_buffer[6] = acquire_spectrum_frame_average_number.to_bytes(1, byteorder='big')
        command_send_buffer[7] = int(acquire_spectrum_enable_auto_exposure).to_bytes(1, byteorder='big')
        command_send_buffer[8] = 0x01.to_bytes(1, byteorder='big') # set active return value
        command_send_buffer[9] = get_checksum_value(command_send_buffer).to_bytes(1, byteorder='big')
        raw_bytes_string = b''
        command_send_bytes_array = bytearray(raw_bytes_string.join(command_send_buffer))
        command_send_bytes = bytes(command_send_bytes_array)
        answer = await client.write_gatt_char(ble_rx_characteristic, command_send_bytes, response=True)


async def main():
    device = await BleakScanner.find_device_by_filter(
        lambda d, ad: d.name and d.name.lower() == TARGET_DEVICE_NAME.lower()
    )
    if device is not None:
        global client
        async with BleakClient(device) as client:
            print(f"Connected: {client.is_connected}")
            svcs = await client.get_services()
            for svc in svcs:
                print(svc)
            print("services: {}".format(svcs))
            await client.start_notify(NOTIFY_CHARACTERISTIC_UUID, notification_handler)


            svc = svcs.get_service(SERVICE_UUID)
            global ble_rx_characteristic
            ble_rx_characteristic = svc.get_characteristic(RX_CHARACTERISTIC_UUID)
            await send_nsp32_ble_command(client, ble_rx_characteristic)
            while True:
                await asyncio.sleep(1.0)
                
            await client.stop_notify(NOTIFY_CHARACTERISTIC_UUID)
    else:
        print("NSP32_SPECTRUM BLE device not found.")

if __name__ == "__main__":
    asyncio.run(main())